package com.progtareas.ProgTareas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProgTareasApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProgTareasApplication.class, args);
	}

}
